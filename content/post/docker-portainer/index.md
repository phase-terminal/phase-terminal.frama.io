---
title: "Docker et appnativefy : exemple avec Portainer"
date: 2021-03-18T01:00:00+01:00
draft: false
---


La dernière fois, j'avais parlé de la possibilité d'élargir le champ des application sous Linux en utilisant *Docker* et *appnativefy*. Je vais vous faire une légère introduction à tous ça par l'exemple en vous montrant l'installation de Portainer Community Edition et une mise en place en tant qu'application autonome pour pouvoir gérer les différents conteneurs sans avoir besoin de monopoliser un onglet dans votre navigateur et pouvoir y accéder facilement. En plus vous avez de la chance, la nouvelle version de *Portainer* prend en charge les version 3 de docker-compose. Elle est pas belle la vie ?


##  Installation de Portainer-ce

Je pars du principe que *Docker* est installé et fonctionnel sur votre machine, l'installation différant quelque peu selon la distribution utilisée. Concernant les volumes, je places les données persistants dans mon répertoire personnel, dans le même répertoire que mon `docker-compose.yml` pour faciliter les sauvegardes, mais libre à vous de modifier l'emplacement des datas.

```shell
docker run -d -p 8000:8000  \
           -p 9000:9000 \
           --name=portainer-ce \ 
           --restart=always \
           -v /var/run/docker.sock:/var/run/docker.sock \
           -v ./volumes/data:/data \
           portainer/portainer-ce
```

A partir de ce moment, si vous n'avez pas l'image `portainer/portainer-ce` en local, il va la télécharger et la lancer.
Une fois ceci fait, rendez vous sur [http://localhost:9000]()

![Création du mot de passe administrateur](./admin.png)

## Mise en place de l'AppImage

Avant de passer à la configuration de *Portainer-ce*, on va le placer dans un appimage. Vous aller voir, c'est très simple, nous avons juste besoin de  *appnativefy*. Il faut cependant avoir `npm` fonctionnel sur son système.

```bash
npm install -g appnativefy
```

Et pour créer le appimage, rien de plus simple

```bash
appnativefy --name="portainer" --url="http://localhost:9000"
```

et vous aurez votre AppImage dans `$HOME/appnativefy` à placer dans `$HOME/bin`, `/opt`, `/usr/local/bin`, vous êtes grand je vous laisse seuls juges.

## Création du script de démarrage

Faire le AppImage ne suffit pas à lancer l'application. Il faut en effet s'assurer que le conteneur soit bien lancé avant de lancer le AppImage, ou vous aurez Une page blanche, celui n'étant qu'un lien vers l'application. Quand on ferme le AppImage, il y a plusieurs solutions :

* Laisser tourner le conteneur normalement ;
* Le mettre en pause (Il n'utilisera pas de cpu mais restera en ram) ;
* L'éteindre.

Pour ma part, je met le service en pause, pour y accéder plus rapidement ensuite. Donc voici le script que je vous propose

```bash
#!/usr/bin/env bash

name="portainer-ce"
cd $HOME/dev/projets/docker/${name}

if [[ ! $(docker ps --filter "name=${name}" --format '{{.Names}}') == ${name} ]]; then
    docker-compose up &
else
    status=$(docker ps --filter "name=${name}" --format '{{.Status}}')
    if [[ $(grep "Paused" <<< ${status}) ]]; then
        docker-compose unpause
    fi
fi

while [[ ! $(docker ps --filter "name=${name}" --format '{{.Names}}') == ${name} ]]; then
do
    sleep 1
done

$HOME/.local/bin/${name}-x86_64.AppImage
docker-compose pause


```

Bien évidemment ceci est à adapter à votre situation. Dans le cas ici, il va retarder l'ouverture du AppImage tant que le conteneur n'est pas entièrement chargé, c'est pourquoi la boucle `while` est dans le script.

##  Le fichier .desktop

Vous voudrez sûrement accéder à l'application graphiquement. Pour cela, on va créer un fichier `.desktop`à placer dans `$HOME/.local/share/applications/portainer.desktop`

```bash
[Desktop Entry]
Version=1.0
Name=portainer
Icon=/home/lomig/dev/projets/docker/portainer-ce/icon.svg
Comment=Manage your Docker containers
Exec=/home/lomig/.local/bin/portainer
Terminal=False
Type=Application
```

Il faudra juste faire une petite manipulation supplémentaire pour l'icône. On va télécharger celle du site internet et l'enregistrer dans le même répertoire que le *docker-compose*

```bash
wget https://www.portainer.io/hubfs/Brand%20Assets/Favicon/Portainer%20BE%20Favicon%20icon.svg \
    -0 ~/dev/projets/docker/portainer-ce/icon.svg
```

![Portainer sous Gnome](./gnome.png)

Comme vous pouvez le voir, si vous affichez vos applications en cours, l'icône d'électron s'affichera également.

## Conclusion

A partir de là, vous avez tout le nécessaire pour utiliser par mal de services docker comme applications. De plus, si vous avez plusieurs machines, vous pourrez en changeant l'adresse ip et en envoyant les commandes par ssh lancer et utiliser les conteneurs que vous voulez. Les possibilités sont nombreuses, faites vous plaisir