---
title: "On the road again ..."
date: 2022-12-11T01:00:00+01:00
draft: false
---

Comme un refrain qui tourne en boucle dans me tête et qui n’en sort pas ; cela fait environ 2 ans que j’ai fait ma première LFS (Linux From Scratch). Depuis, ça me hante, ça m’obsède. Il faut que je la fasse. La dernière (ou plutôt la première) etait une sorte de proof of concept, une ébauche, un test grandeur nature si vous voulez. Juste un truc qui boot, et encore sans réseau ! C’est pour ça que ça me laisse un goût d’inachevé.

Alors bien évidemment, entre faire une LFS et une BLFS (Beyond Linux From Scratch, projet qui permet d’aller au delà, notamment en installant la partie graphique Xorg et un DE) la route est longue ! Et il ne me viendrai pas à l’idée de le faire sans les outils appropriés.

Il me faut donc une solution pour gérer des paquets. Dans les gestionnaires de paquets que je connais, pacman est celui que je maitrise le mieux. Bien qu’il y ai quelques comportements que je n’aime pas, j’ai essayé de l’installer sur une LFS.

Installation OK, sauf quelques soucis :

- Soit je veux créer et installer un paquet dans la LFS et il faut y aller à coup d’overwrite, sans compter qu’il faut aller très loin dans LFS pour que pacman soit disponible ;
- Soit je l’installes plus tôt, et là je suis resté bloqué n’arrivant pas à lancer un pacstrap. J’avoue que je n’y ai pas passé des nuits blanches non plus, mais je me dis que c’est le karma et qu’il me faut autre chose.

Je pense alors à créer un gestionnaire de paquets. J’en avais déjà fait un, ou plutôt un ersatz en bash. Fonctionnel et très basique certes, mais bash pour un gros projet, pas pour moi. Certains l’ont fait, comme la Kiss Linux ou le projet lfs-me sur Github, certes, mais je préfères autres chose.

La première question est en quel langage ?

- Un langage compilé permet de créer un binaire qu’il est très facile d’inclure dans l’arborescence. On peut citer C/C++, Rust, Go …
- Un langage interprété n’as pas besoin de compilation, et peut poser des soucis dans le cas de la montée en version dudit langage. Faut pas que ça fasse péter le projet, ni aucune des dépendances d’ailleurs !

Vous l’aurez compris, un langage compilé, c’est surement le top, mais voilà, en langage, je suis plutôt limité : Bash et Python. C++ je bricole, mais à un moment ça coince (putain de pointeurs ….). Rust et Go, à vrai dire, j’ai pas vraiment essayé.

Du coup ce sera Python, et ce sera très bien comme ça. Il y a des jours ou ça végète, et depuis que j’ai relancé le projet en Python, ça avance. Miraculeusement

Du coup, il me faut plusieur choses :

- Un espace pour gérer les différentes versions du futur OS: un espace d’expérimentation et de tests, l’OS en développement, et la version de production. Et donc un outil permettant de créer un template de nouvelle version, passer un soft de dev en prod. Bref quelques petites fonctions qui pourront se révéler pratiques ;$
- Un outil pour gérer les fichiers de build ou “recettes” : gestion de template, vérification du bon formatage, checksum des différents fichiers, incrément du numéro de version et de révision, checksum sur la page de dl du soft pour être prévenu d’un changement (Ce qui peut indiquer une nouvelle version)
- Un outil de gestion de mirroir : Ajout, suppression de soft, liste des paquets
- Bien évidemment un outil pour créer les paquets
- Plus bien sur quelques librairies “utilitaires” à créer.

Il va sans dire que la route va être encore plus longue si je créé l’intégralité des outils, mais il y a tellement de chose que je souhaiterai faire que ça peut en devenir intéressant. Car les distributions actuelles ne sont pas pensée pour être maintenu par une très petite équipe, voire une personne (sauf Slackware bien entendu !)

Le seul soucis, c’est que je suis un peu rouillé niveau code. Ça revient vite, mais j’ai besoin de me décrasser le cerveau si je peux me permettre l’expression.
