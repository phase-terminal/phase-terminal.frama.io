---
title: "Mon expérience LFS"
date: 2021-03-08T01:00:00+01:00
draft: false
---

En ce moment où j'ai personnellement beaucoup trop de temps disponible, je m'étais lancé le défi de me faire une Linux From Scratch. Après tout, pourquoi pas. Je commence à avoir l'habitude de la Gentoo, je vois à peu près le temps de compilation, j'ai du temps à tuer et en plus j'ai un disque dur externe de disponible. Je précise que j'ai mené cette expérience sur trois  semaines au mois de décembre et que cet article est donc une réaction à froid de la chose. Je sais que certaines personnes le font en trois jours, mais vous allez comprendre. Pourquoi en parler aujourd'hui ? Tout simplement le souvenir d'une blague ou j'avais balancé «Si pour 40 ans, tu n'as pas fais une LFS, alors tu as raté ta vie.» Alors quoi de mieux que le jour de mes 40 ans pour en parler ?

## Les débuts

Je pars donc sur une LFS 10.0 avec sysvinit. Je commence à suivre les indications de préparation des partitions et de la base de compilation. Concrètement tout se passe bien, le livre est clair bien que certain préconisent quand même de préférer la version anglaise apparement plus précise sur certains points, je n'ai rencontré aucuns problèmes. Finalement c'est assez rapide, mais il y a un hic ou plusieurs qui viennent à ce moment là :

* il faut rester devant son PC faire toutes les manipulations au fur et à mesure : décompression de l'archive, configure, make, make install et éventuellement des patchs à appliquer, voire des actions de post installation
* On ne fait que du copier-coller. Ouais, dit comme ça, c'est pas très sexy et ça ne donne une sensation amère de non accomplissement. Au final, faire 3 jours de copie-coller, il me faut autre chose pour pimenter tout ça.

Je recommence alors tout en scriptant le tout, mais bon au final, vu la gueule du script, ça marche, mais c'est juste un autre copie-coller du bouquin. Très verbeux, redondant et absolument une vraie merde à maintenir.


## Le gestionnaire de paquet

Passer par un gestionnaire de paquet parait donc la suite logique de l'expérience. Au début, je part dans un esprit simple que je connais, le Bash. Si c'est pour faire des appels systèmes pour configure, make et make install, Python n'est pas forcément la meilleure chose, un prototypage en bash sera largement suffisant. Je part donc sur une base que je connais et qui a des fichiers de définition en shell, les PKGBUILD d'Archlinux. Étant breton et fier de l'être n'est ce pas, le bretonise le tout :

* Le gestionnaire de paquet s'appelle `pak` — paquet
* Le répertoire de définitions s'appelle `rekipe` — recette. À la différence d'Archlinux, je n'ai pas fait de répertoire avec un fichier `REKIPE`, mais un fichier `paquet.rek`.
* Le répertoire contenant les archives de code source s'appelle `tarzh` — archive. 

Et je colle tout ce beau monde dans `/var/cache/pak`

Et c'est parti mon kiki. Je commence donc par faire un bout de script, objectif récupérer les sources et les patchs et faire le boulot de compilation. Mais il faut toujours attendre.Qu'a cela ne tienne, je passe la compilation dans un fakeroot, lje compresse le tout et j'en fais une archive `paquet.pak`. J'en profite pour y glisser la liste des fichier installés, toujours pratique en cas de mise à jour ou de suppression. le répertoire `stal` — marché — fait également son apparition pour stocker tout ce beau monde. 

Mine de rien, ça demande un peu de temps, et quelques paquets demande une adaptation du fait du fakeroot. Je dois avouer que je suis resté bloqué avec deux paquets, `gcc` et `glibc` que j'ai continué à faire à la main.

## Passons à la suite

C'est bien beau de programmer, mais il faut peut être aussi avancer le projet. J'avais les sources, donc je les copies dans le chroot, je jance pak qui me fait les paquets les un après les autres. Je tentes même de refaire de zéro la chaine de compilation avec les paquets compilés, temps total moins de deux minutes grâce à pak : impeccable. Et c'est là que viens un pnoblème majeur non perçu. Il me faut quelques chose pour télécharger les paquets une fois arrivé dans le chroot.

## Téléchargeons les sources

J'ai pour faire simple deux solutions :

 * Faire en sorte d'avoir wget dans le chroot ;
 * Réécrire pak dans un langage compilé pour embarquer une bibliothèque comme curl.

Et vous savez quoi ? Je me suis dis, « J'ai jamais eu l'occasion d'apprendre le C, je tente le coup ». Résultat des courses, 3 jours à me dépatouiller dans le C pour télécharger les sources. Je n'arrivais pas à lui faire lire le fichier rekfile correctement. Une horreur.

J'ai donc laissé tomber cette option, et je me suis lancé dans la préparation de wget qui fait parti de BLFS. Wget qui a besoin de dépendances, qui ont elles même besoin de dépendances. Une dizaine de paquets plus tard, ça y est, wget fonctionne. Enfin, pastotalement. Arrivé à la première source spécifiée en https, il ne veux rien savoir. Je cherche, je trouve, il ne faut pas grand chose et au final, ça fonctionne. En quelques heures c'est bouclé.

Une fois ceci fait, j'oscille entre la compilation de la LFs et l'optimisation de pak en essayant de télécharger les sources dans un thread à part. Sur la fin, je me dis que le projet étant très personnel, il y a peu d'intérêt pour les binaires et j'enlève tout ce code de pak. Je repart sur du simple et efficace.

## Le noyau

Arrivé à la fin, le défi ultime de la LFS : le noyau Linux. Autant vous dire que c'est coriace. En même temps, le faire sur un disque ssd externe en USB-C, personnellement, je vous le déconseilles. Si je me souviens bien, j'ai du arriver à 14 ou 16 compilation avant de booter. Plusieurs jours de documentation,de recherches, de tests, mais certainement la partie la plus formatrice du projet. 

![Gwen Erminig est née](./lfs.jpg)

Mais arriver au log et ouvrir vim, est une sensation unique à ce moment. Le moment ultime étant l'inscription sur le site de Linux From Scratch. J'ai l'Id **28646** sur la page d'enregistrement des utilisateurs LFS.

## Au final, que retenir de tout ça ?

* Les parties les plus intéressantes étaient de loin la création du gestionnaire de paquet et la mise en place de wget où il fallait se détacher du livre et clairement chercher et se démerder.
* Suivre le livre en faisant un copié coller n'est pas très intéressant et ne vous apportera pas grand chose. L'intérêt vient du moment ou il faut faire des choix.

## Vers une BLFS ?

Dans l'euphorie de la chose, cela m'a traversé l'esprit, je ne vais pas le nier. Mais pour ça, il me faudrait du multilib, et c'est encore une autre paire de manche, car encore plus de paquets.Dans l'éventualité ou je l'aurais fait, je pense que je serai parti sur une fixed annuelle, avec an développement commençant avec une nouvelle version de `gcc`.Mais il faut avoir les pieds sur terre. J'ai une Gentoo, et l'éventualité de la réinstaller ne m'enchante guère, alors une BLFS … 

Néanmoins, c'est une expérience très intéressante et un formidable sujet d'étude à faire au moins une fois dans son expérience linuxienne, pour peu que vous en ayez l'envie.
