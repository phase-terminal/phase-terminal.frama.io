---
title: "Vive le distro-hopping !"
date: 2021-03-15T00:00:00+01:00
draft: false
---

Là, je sens que je part sur un terrain glissant truffé d'embuches. C'est bien connu, le distro-hopping, c'est mal. Très mal. Utilisé par les incompétents incapables de lire un wiki, qui changent de distributions comme ils changent de chaussettes. Mais le distro-hopping est il vraiment mauvais ?

Clairement si vous changez toutes les semaines, vous allez perdre plus de temps qu'autre chose. Le changement de distribution est fait pour combler un manque et rien que ça. La personne n'est pas capable des résoudre un problème ou rage de ne pas avoir sa killer-feature disponible dans la distribution précédente. Au final, ça tourne en rond pour les mauvaises raisons.

En deuxième lieu, il y a la volonté de comprendre et d'apprendre. Je fais parti de ceux qui aime Linux et qui souhaitent en apprendre plus sur les arcanes du système. Savoir comment ça marche. pour faire simple, mon parcours est dans les grandes lignes :

* Ubuntu
* Debian
* Fedora
* Arch Linux
* Gentoo

Le saut de Ubuntu à Debian était pour enlever une couche, se rapprocher de la maison mère. Mon expérience n'était pas forcément suffisante. A l'époque, je me rappelle que je serrai les fesses à chaque mises à jour de Xorg dans Sid. J'avais fais le mauvais chois de prendre une carte Nvidia, et il pouvais arriver que ça pète sans prévenir. Un terrain un peu casse gueule, mais formateur par rapport à Ubuntu. J'avais même expérimenté un serveur mail self hosted, que j'ai laissé tombé au bout d'un an, mais l'expérience était très intéressante.

Je suis ensuite passé sous Fedora. On retrouve la simplicité d'utilisation d'une Ubuntu, ça fonctionne très bien, c'est solide comme un rock. J'appréciai notamment la gestion des paquets python et LaTeX qui permettait de se passer de python-pip et n'obligeait pas à installer un gros méta paquet LaTeX alors qu'on utilise au final qu'une petite portion de celui ci. Entre plus de 6000 paquets d'un côté et 80 de l'autre, l'installation est plus sélective. Seulement à l'époque sur un portable avec un disque 5400 tours/min, le passage d'une version à une autre était long...

J'ai ensuite voulu comprendre un peu plus le système. Passage sous Archlinux que j'ai connu tardivement. Je me souvient que j'étais tombé sur cet article de blog : https://www.p3ter.fr/installer-et-configurer-archlinux.html. J'ai tenté le coup, et ça s'est plutôt bien passé. Bien évidemment, mon instinct de bidouilleur compulsif m'a fais réinstaller Arch à plusieurs reprise, soit pour tester un script bash de réinstallation, soit pour tester des partitionnements différents, btrfs lvm notamment, soit pour personnaliser plus l'installation notamment au niveau des userdirs, prise en charges des outils de décompression multi-coeurs, utilisation d'un dépôt personnel pour me passer du hasardeux AUR.

Pour finir, je suis passé sous Gentoo. Pour pouvoir personnaliser un peu plus le système, me frotter à la compilation de noyau qui est très instructive, pour me passer de systemd que je trouve trop massif. L'adoption de ce système de n'est pas fait sans heurts, loin de la, mais c'est la le meilleur apprentissage que l'on peut avoir. Learn the hard way. Personnellement il n'y a rien de mieux. Se confronter aux difficultés, chercher des solutions, comprendre, appliquer. C'est comme cela que j'avance.

Alors bien sur entre chaque transitions, il y a souvent un retour en arrière. Un essai de quelques mois, où on fini par stagner, retour sur la distribution précédente histoire de laisser mariner tout ça ou passage express sur une autre, et le temps faisant, on y revient les idées plus claires, on sait ce que l'on veut, et surtout ce que l'on veut pas, et on passe le cap. 

On va surement me répondre que c'est une perte de temps, une perte de données. Personnellement mes backups sont fait, et lorsque je réinstalle, je retrouve une session utilisable rapidement. Maintenant c'est encore plus simple, toutes mes données sont sur un raid que je monte dans $HOME/Documents, et basta. 

Le dernier test en date était une Linux From Scratch, un mois à tester l'installation, créer un gestion de paquet en bash, préparer la chaine de compilation, toujours dans la même optique, comprendre, apprendre, avancer. Une première version à été faire, à l'aide de scripts, la gestion de la création à été modifiée, j'ai pris quelques paquets de Beyond Linux From Scratch pour pouvoir utiliser le gestionnaire de paquet maison ; le téléchargement des sources nécessitait de  pouvoir utiliser wget sur des adresses https. Au final, une version bootable avec un gestionnaire de paquet basique mais fonctionnel et Vim. C'est tout ce qu'il y avait.

Depuis je n'y ai pas vraiment touché. Je ne dit pas que je ne vais pas m'y remettre. Comme souvent, ça va infuser dans le cerveau quelques temps, et je recommencerai surement un jour, avec comme base une réécriture du gestionnaire de paquet. Et je passerai surement un autre cap pour avancer plus dans la version Beyond Linux From Scratch. De la à passer durablement dessus, j'ai le temps. Pour l'instant je préfère optimiser l'administration de la Gentoo.

En fait, ce que je veux faire comprendre, c'est que toute expérience est bonne à prendre. Testez, testez, il en restera forcément quelques chose. Encore faut il changer pour les bonnes raisons. Changer pour garder la même base et avec quelques modifications de configurations, je ne vois pas vraiment l'intérêt. Dans les distributions similaires, on pourrait noter :
* ArchLinux, EndeavourOs, ArcoLinux ;
* Toutes les variantes Ubuntu, alors qu'on peu changer d'environnement de bureau facilement ;


A la limite, plutôt que de changer, essayer de modifier les images disques des distributions, et faire la votre, correspondant à votre matériel et à votre usage. Je ne dis pas qu'il faut distribuer votre travail, et polluer encore un peu plus distrowatch qui ces temps ci croule sous les installateur Archlinux, mais le garder pour vous comme une sauvegarde.

Les outils intégrés aux distributions sont généralement bien fais, avec pour les plus accessibles :

* Manjaro avec les Manjaro-tools ;
* Fedora avec les fichiers kickstarts ;
* Archlinux avec Archiso ;
* Ubuntu avec des projets officieux mais qui ne sont pas forcément pérennes.

Ou vous avec la solution de le faire à la main, créer votre solution maison. Plus long, plus chiant certes, mais tellement plus formateur ! 

Pour faire simple, changer pour contourner un problème, n'est que retarder l'inévitable échéance, changer encore pour contourner un problème. Changer pour parfaire son bagage technique en est une autre. A vos claviers !
