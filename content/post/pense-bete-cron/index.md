---
title: "Pense Bete Cron"
date: 2021-03-11T01:00:00+01:00
draft: false
---

Cron sert à automatiser des taches, mais bien évidemment, quand vient le moment
de l'utiliser, c'est souvent le trou noir. Un petit schéma et c'est parti !
Mais afin d'être sur que vous ne ratiez pas ce point qui peut causez quelques
soucis, nous allons commencer par ça :

## A bien retenir 

**Cron lance les scripts avec sh et non bash !**

Donc si vous avez un script bash à lancer, faites le comme ceci

```
* * * * * bash mon_script.sh
```
Autrement c'est la plantade et vos fichier de logs vont gonfler comme pas
possible. Et si vous souhaitez que les erreurs ne nourissent pas les logs,
suffixez votre commande avec

`> /dev/null 2>&1`

```
* * * * * bash mon_script.sh > /dev/null 2>&1
```

Normalement votre script gère les erreur lui même n'est ce pas ? Donc pas besoin
de faire doublon. Après s'il n'est pas lancé, vous vous en rendrez bien compte.

### Les 2 commandes à connaitre

En faite il n'y en a qu'une, mais deux arguments possibles.

```
 # Éditer les taches cron
 crontab -e

 # Visualiser les taches cron
 crontab -l
```

### Et enfin

**Vérifiez que le service cronie est bien lancé !** Maintenant passons aux choses sérieuses. 

## Schéma de cron


```
*   *   *   *   *   Commande à lancer
|   |   |   |   |
|   |   |   |   |__ Jour de la semaine 0 (Dimanche) à 6 (Samedi) 
|   |   |   |       7 vaut pour dimanche également
|   |   |   |   
|   |   |   |______ Mois (1 - 12)
|   |   |   
|   |   |__________ Jour du mois (1 - 31)   
|   |
|   |______________ Heure (0 - 23)
|
|__________________ Minutes (0 - 59)

```

### Quelques exemples

```
 # Toutes les heures à 10 minutes
 # 0h10, 1h10 etc …
 10 * * * * 

 # À Noël
 0 0 25 12 * bash balance_les_cadeaux.sh > /dev/null 2>&1

 # Un 29 février si celui ci tombe un jeudi
 0 0 29 2 4 bash joyeux_anniversaire.sh > /dev/null 2>&1
```

## Mais ce n'est pas tout !

Et oui, il n'y a pas que les astérisques dans la vie ! Il y a aussi : 

- Le tiret (**-**) : Pour définir une portée. 1-5 dans la colonne des jour de la semaine signifie du lundi au vendredi
  inclus

```
 # Pour un réveil en douceur à 6h du matin
 # du lundi au vendredi
 0 6 * * 1-5 bash va_bosser_feignant.sh
```

- Le slash (**/**) : Pour définir une incrémentation dans la portée. **\*/5** dans la colonnes des minutes signifie
  toutes les 5 minutes.

```
 # Pour un réveil en douceur à 6h du matin avec répétition toutes les 5 minutes
 # du lundi au vendredi pour un total d'une demi heure
 0-30/5 6 * * 1-5 bash va_bosser_feignant.sh
```

- La virgule(**,**) : Pour séparer les valeurs distinctes, on peut utiliser la virgule. Donc si on a la chance de ne pas
  travailler le mercredi, on peut faire
  
```
 # Pour un réveil en douceur à 6h du matin avec répétition toutes les 5 minutes
 # du lundi au vendredi, mercredi exclu, pour un total d'une demi heure
 0-30/5 6 * * 1,2,4,5 bash va_bosser_feignant.sh
```

Vous voilà paré pour bien vous réveiller et bien démarrer la semaine !


