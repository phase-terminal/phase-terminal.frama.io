---
title: "Plus d'applications grâce à Docker et appnativefy"
date: 2021-03-04T01:00:00+01:00
draft: false
---

Certaines personnes se plaignent du manque d'applications sur Linux. Il existe pourtant une solution qui n'est certes pas la plus conviviale, mais qui, aux prix de certains efforts, peut valoir le coup. Cette solution s'appuie sur un nombre incalculable d'applications qui peuvent être auto-hébergées.

Je sais, votre intention n'est pas de créer un cloud personnel, et je le comprend, mais faut il pour autant mettre de côté toutes ces applications ? À mon sens, absolument pas, car croyez moi, il y a quelques pépites

# Principe

Beaucoup de ces applications utilisent Docker pour pouvoir être déployées. Docker n'est pas obligatoire, mais dans le cas du projet Overleaf par exemple, si vous voulez vous amusez à configurer mongo et redis, libre à vous, avec docker, le boulot est déjà fait. La seule chose à faire est généralement de cloner un dépot Git, créer l'image Docker et encapsuler le tout dans une AppImage.

La raison de la création de l'AppImage est de ne pas avoir à utiliser l'application dans le navigateur, car utiliser simultanément plusieurs applications veux dire plusieurs onglets. Vous voyez le problème. C'est quand plus simple d'avoir une application dans sa fenêtre propre.  

![./apercu des applications avec appnativefy](./apercu.png)

## Quelques avantages de Docker

* Le déploiement est simplifié : 

    Pas besoin de tout configurer à la main. Vous avez au maximum deux commandes à lancer. la plupart des projets ont des images déjà prêtes, pas besoin de réinventer la roue.

* La sauvegarde des données :
     
     En effet, il suffit de spécifier ou monter vos volumes Docker (Données des applications) et le tour est joué. Par défaut, tout est placé dans `/var/lib/docker` mais vous pouvez choisir le chemin par applications. Personnellement, je met tout dans mon dossier utilisateur, chaque applications dans un répertoire, ce qui facilite une éventuelle remise en route en cas de catastrophe.

* Homogénéité de fonctionnement : 

    J'ai personnellement un fixe sous Gentoo et un portable sous Fedora. Et ça marche de la même manière. Je fais un montage sshfs du répertoire ou sont les données et je peux travailler tranquillement sur l'un au l'autre poste. Si je pars en week-end, une synchro, et le tour est joué, je retrouve les données sur le fixe.


## Pourquoi appnativefy ?

C'est un projet très récent sur Github. Mais qui m'a tout de suite intéressé. La problématique de docker, c'est que chaque service a son propre port et donc on se retrouve avec des adresses type http://localhost:9000, http://localhost:3001, etc …
Avouez que pour vous y retrouver, ce n'est pas le plus simple. Il reste la solution du reverse proxy vous allez me dire, mais quand on a un outil qui facilite grandement les choses, autant en profiter.

De plus, comme expliqué plus haut, pas besoin d'ouvrir plusieurs sessions de votre navigateurs, vous ouvrez juste un AppImage. Pas d'onglets, pas de barre de navigation, et les liens s'ouvrent externes s'ouvrent dans votre navigateur. On est d'accord qu'il y a très peu d'intérêt a mettre un site web, mais des applications oui. Si vous avez une application de bookmark, ceux ci s'ouvriront naturellement dans votre navigateur.

## Pourquoi ne pas prendre des applications natives Linux ?

Clairement, parce que vous n'avez pas beaucoup de choix dans certains cas :

* Kanban : 
  
  Quasi inexistant ;

* Favori Web : 

    Pour être clair, le module intégré de Firefox est pas forcément le truc le plus intuitif au monde. Et je n'ai pas envies de m'inscrire à un service pour le faire ;

* Portainer :

    Utilisé pour gérer les images Docker simplement. Pas vraiment d'équivalents ;

* Flux Rss : 

    Je préfère clairement l'ergonomie des solution web. Peut être une question d'habitude ;

* Client Git :

    Comme pour les flux rss, je préfère leur ergonomie. Et centraliser les Git quelques part, permet de travailler facilement avec plusieurs machines. Pour un suivi des quelques soucis qui peuvent intervenir, la gestion des issues est suffisante, pas besoin d'un autre outil.
    
* Et bien d'autres …

Et voir aussi que si un jour je souhaites faire de l'auto-hébergement, ce qui est une chose qui m'attire de plus en plus, les outils seront déjà la, et la mise en place facilitée.

## Quels inconvénients à tout ça ?

 * Le temps de mise en place.
 
     Oui, ça demande un peu de temps et d'organisation. Préparer le conteneur Docker en lui même, préparer l'AppImage, préparer les exécutables de lancement.
     
* Il faut apprendre à utiliser Docker:

     Bien qu'il n'y ai rien de compliqué, c'est une étape à franchir, mais si vous êtes la, c'est que ça ne vous dérange pas d'en savoir plus, n'est ce pas ?
     
* La taille des images : 
        
     Forcément, ça prend plus de place que si vous le faisiez en natif sur votre système. Mon répertoire `/var/lib/docker` fait dans les 7 Go, mais quand on voit la taille des disques dur aujourd'hui, ce n'est pas franchement la mer à boire. 
     
 
 ## À venir sur le blog 
 
 Je vais pas vous inonder aujourd'hui d'informations Dans un premier temps, je vous présenterai **appnativefy** qui est d'une simplicité enfantine et que vous pourrez commencer à utiliser même si vous n'avez pas encore de services Docker.
 
 Viendront ensuite les explications sur **Docker**, avec des exemples d'applications.
