---
title: "Workflow du blog"
date: 2021-03-01T01:00:00+01:00
draft: false
---

Lorsque j'ai voulu lancer ce blog, j'ai cherché à avoir une certaine organisation, pour avoir des idées en stocks, un planning, des note pour préparer des articles, pour gérer l'outil en lui même. 

Tout commence par l'outil. Le choix d'une générateur de site statique hébergé dans un premier temps sur Framagit me permet la meilleure chose à faire, tant au niveau des performances, du suivi dans le temps, de pouvoir avec un Workflow avec Git. Un éditeur markdown est suffisant pour éditer les articles. Je prépare mes articles avec l'aperçu de mon site qui tourne juste à côté de l'éditeur ; il s'agit de [Zettlr](https://github.com/Zettlr/Zettlr), ce n'est peut être pas le plus léger, mais très agréable à utiliser et le confort est primordial quand viens le temps de poser ses idées, bien que la plupart du temps, je le fait au calme sur le canapé, à l'ancienne, avec papier et crayon. Je rature vite, appose des notes, réorganises facilement ; je trouve cette méthode plus instantanée et peaufine ensuite avec l'éditeur. 

![pour la forme, une copie d'écran d'une édition d'article en cours.](./capture.png)

Pour le déploiement Git, il y a 3 dépôts :

* Le principal avec la configuration générale d'[Hugo](https://github.com/gohugoio/hugo) ;
* Un autre dépôt pour le thème ;
* Un dernier dépôt pour les articles.

Si un jour je veux changer de thème, c'est simple, je peux travailler tranquillement sur une autre branche, sans provoquer de changement sur le site en production. Si je change de générateur de contenu, j'ai mes articles dans leurs propre dépôt. Juste les entêtes yaml à modifier,  et ce sera bon.  Concernant les articles, Les articles en cours d'écriture sont dans leur propre branche, et je merge dans master quand l'étape de relecture est faite. Avec Gitlab, il me suffit alors de lancer un build avec mon téléphone le jour venu pour publier un article la date de publication étant spécifiée dans l'entête.



## Alors pourquoi Hugo ?

Il est rapide, il est complet, il recharge à la volée le contenu dans le navigateur dès qu'il détecte un changement dans  les fichiers markdown. Couplé à Zettlr qui sauvegarde automatique et régulièrement les fichiers, on voit l'aperçu de l'article apparaître au fur et à mesure. Alors certes, et je le confesses volontiers, je ne me suis pas encore plongé dans les entrailles de Hugo. J'ai juste préparé un Dockerfile, pris un thème sans fioriture, faisant passer l'écrit en premier lieu. Si prochainement je souhaites ajouter d'autres parties au site, je le pourrai aisément. Ce n'est pas qu'un moteur de blog, mais un framework complet. De plus Hugo est compatible avec Gitlab Pages, donc nul besoin de synchroniser le site complet, mais juste la partie avec les articles en markdown.


## Les autres outils complémentaires

Bien sur, j'utilise d'autres outils bien pratiques pour m'organiser sereinement
 
 ### Un kanban
 
 J'utilise un Kanban du nom de [Planka](https://github.com/plankanban/planka). Pourquoi lui ? Honnêtement, c'est un des premiers que j'ai trouvé, il fonctionne très bien avec Docker, se lance directement grâce à un fichier `docker-compose.yml` déjà prêt. Je n'ai pas besoin de fioritures, étant seul à la barre. , mais ça me permet de noter : 
 
 * Les idées à développer ;
 * Les ébauches commencées ;
 * Les articles à relire ;
 * Ce qui est prêt à être merge & push ;
 * Les articles sur le dépôt en attente de build.
 
 Sur chaque fiches d'articles, Le titre, un ou 2 tag, et la date de publication programmée. Il est important, pour moi, d'avoir une visualisation d'ensemble, pour savoir où je vais. J'ai déjà lancé des projets sans organisation, je sais trop bien comment ça fini. Trop d'idées tue l'idée, j'ai du mal à mettre des priorité, et c'est le début de la procrastination. Au final le projet est au point mort,rien ne sort.  
 
 ### Un gestionnaire de marque-page
 
 Mon choix s'est porté sur [Linkding](https://github.com/sissbruecker/linkding). Il n'a rien de révolutionnaire, ça fonctionne comme le vieux service Delicio.us. Un lien, des tags, une belle réserve, et quelques idées d'articles à piocher dedans. En cas de panne sèche, ça peut servir. Au début j'étais parti sur Lobster, qui fonctionne différemment, comme le journal du hacker, mais j'ai rencontré quelques difficultés avec Docker et notamment le conteneur de sa base de données.
 
 
 ### Quelques outils complémentaires
 
 Il peux m'arriver d'utiliser des outil de mind-mapping. Personnellement je suis tombé sur Xmind qui n'est pas libre, je vais voir si je peux trouver autre chose. Il faut que je test Freemind à ce sujet. Pour des prises de notes plus complexes, j'ai également [Micropad](https://github.com/MicroPad/MicroPad-Core) qui permet d'utiliser plusieurs médias, texte, image, sons, vidéo etc … Pas encore utilisé, mais je le garde, au cas où.
 
 Pour en finir avec tout ça, j'utilise également [Ungit](https://github.com/FredrikNoren/ungit) Pour passer d'un article à un autre si jamais j'en édites plusieurs et ce de façon très convivial. Car même quand ce workflow qui peut sembler complexe, il est également possible de pencher vers la simplicité. 
 
 
