---
title: "Pourquoi ce Blog"
date: 2021-02-26T00:00:00+01:00
draft: false
---

J'ai commencé à utiliser Linux il y a un peu plus de 15 ans, en août 2005 pour être précis. J'ai débuté avec la Ubuntu 5.04. Bien évidemment, il y avait quelques petits soucis de mise en place, tout n'était pas immédiat ; il a fallu chercher, être patient, faire preuve d'humilité face à un système d'exploitation que je ne connaissais pas. La contrepartie était de pouvoir s'éloigner de Windows, qui déjà à l'époque posait les pions avec les prémices de récolte d'information. Passer sous Linux avait un coût, mais amenait la récompense d'une certaine indépendance.

Je suis resté sur des distributions basées sur Debian un certain temps en alternance de temps en temps avec Fedora. Naturellement, j'ai commencé à m'intéresser au système en lui même. Dès le début, je faisais les mises à jour
en ligne de commande, et il fallait également passer par là pour la gestion du wifi avec *wpa_supplicant*, pour installer les pilotes Nvidia, ainsi que pour configurer *xorg.conf*. Tout simplement parce que les outils graphiques n'existaient pas. 

Il a donc fallu apprendre, et pour apprendre il faut 2 choses : de bons profs et de bons élèves. Ça parait simple comme ça, mais ce n'est pas du tout évident. Quoiqu'il en soit, le forum ubuntu-fr, le seul forum finalement ou j'ai réellement passé du temps m'a bien servi. Le temps passant, on change de distribution, on trouve de l'aide différemment (article de blog, wiki, man, */usr/share/doc*), l'art de la débrouille façon the hard way.

Aujourd'hui, j'ai l'impression que les personnes viennent sur Linux pour les mauvaises raisons ou ne savent pas dans quoi ils s'embarquent. Donc je vais être clair et net :
* Linux n'est ni Windows ni Mac ;
* Linux ne reconnaît pas tout le matériel nativement ;
* Linux a son organisation et écosystème, il faut abandonner certains réflexes ;
* Une distribution quelle qu'elle soit ne fait qu'une installation *par défaut*   et ne peut convenir à tous. Il peut y avoir des ajustements à faire, ce qui   n'est pas représentatif de la qualité de ladite distribution.
  
Beaucoup de personnes ne veulent plus apprendre, du moins certains. Avec le temps, j'essaie de filer un coup de main, mais on ne peut forcer personne ; c'est la raison d'être de ce blog. Être potentiellement accessible à ceux qui cherchent. 

Trop de monde recherche en Linux un Windows gratuit pour accéder à Google, Facebook, Instagram et consorts. Si vous installez Linux sur un PC dans le but d'utiliser du code libre, c'est une très bonne chose, mais posez vous la question de vos données. Mais ça, ce sera pour un prochain article.

## L'appel de la curiosité

Je ne pense pas qu'il faille potasser GNU/Linux comme on travaille ses cours quand on est étudiant, qu'il faille être
studieux, prendre des notes (quoique), apprendre plein de choses par cœur. Cependant il y a certaines choses à connaître, non pas par obligation, mais tout simplement parce que ça vous rendra service bien plus tard.

Il y a une comparaison qui me fait souvent rire avec certains Youtubeurs, bloggeur à propos de Linux, qui dis en substance :
> Linux c'est comme pour une voiture, quand j'en achète une, j'ai pas envie de savoir comment ça marche, et s'il y a un problème, je vais chez le garagiste.

Soit. Partons de là.

### La formation initiale

On est d'accord pour dire que l'on est pas tous mécaniciens, et que ça intéresse une minorité de personnes. Mais vous avez sûrement oublié que pour pouvoir conduire une voiture, vous avez :
* pris des leçons de code ;
* passé l'examen du code ;
* pris des leçons de conduite ;
* passé l'examen de conduite.

Ou total, ce sont environs 40h de formation initiale pour pouvoir passer l'examen de conduite, qui mène au sésame du permis. Suite à ça, si vous faites les cons, on se prend des amendes, on perd des points, on paye des stages pour récupérer des points, etc …

Que ce soit Linux ou une voiture, je suis d'accord sur un point : l'entretien doit être réduit au minima dans les deux cas de figure, il faut quand même apprendre un minimum de chose. Dans le cadre d'une voiture, il faut être capable de vérifier les niveau du moteur ainsi que le pression des pneus par exemple. C'est chiant, mais c'est comme ça, et mieux vaut prévenir que guérir.

Donc si Linux est comparable à une voiture, et que l'on éduque la personne entre le siège et le volant, je vous propose d'éduquer dans le cadre informatique la personne se situant entre la chaise et le clavier.

## Éducation donc

Je vais donc vous montrer comment vérifier les niveaux, la pression des pneus, car c'est un minimum. Je ne peux accepter le fait qu'on me dise que s'il faut installer les codecs c'est inadmissible et que donc la distribution en question c'est de la merde ; ces distributions ne font que respecter les lois en vigueur du pays dans lequel elles sont installées. Si des lois sur des brevets logiciels existent, elles ne font que les appliquer, et un tel «manque» ne détermine en rien la qualité intrinsèque de la distribution. Et ceci est un exemple parmi tant d'autres.

De plus, savoir utiliser au minimum le terminal permet un autre avantage. Une ligne de commande à copier sera toujours plus compréhensible que «Tu ouvre ça, tu clic la, tu coche tel cases, etc…» 

Bien sur, il ne faut pas tout connaître par cœur, mais avoir des notions vous aidera même à chercher la solution. Quand on ne sait pas de quoi on parle, on est sensé chercher quoi sur son moteur de recherche ? A partir de la, le constat est implacable, vous êtes fichu.

Juste des notions, rien que des notions. Savoir que ça existe, pour que vous puissiez vous dire, «Tiens, ça je l'ai vu quelques part, ça me parle.» Rien de plus. Le but n'est pas de faire une compagnie de singes savants partant se faire mousser. Certains de vous finiront probablement dans le club privé *I use arch BTW* mais ce sera une minorité.

Le seul but de ce blog est de faire de vous des personnes **autonomes**. Rien de plus. Et cette **autonomie** vous mènera au final à la destination de votre choix.

